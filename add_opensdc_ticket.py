import argparse
from add_opensdc_ticket_lib import add_ticket_to_sourceforge, read_comments

parser = argparse.ArgumentParser(description='Add tickets to Sourceforge openSDC project from JSON comments file')
parser.add_argument('bearer',
                    help='Bearer token to get authorized. Create one from https://sourceforge.net/auth/oauth')
parser.add_argument('milestone',
                    help='Defines the Sourceforge milestone, value needs to match the milestone Name')
parser.add_argument('draft',
                    help='Defines the draft in which the issue has been found')
parser.add_argument('ticketid',
                    help='URI part that reflects the tickets identifier, e.g. p11073-10701')
parser.add_argument('-u,', '--user',
                    dest='user',
                    default='d-gregorczyk',
                    help='Defines the assignee, default is d-gregorczyk')
parser.add_argument('-f,', '--file',
                    dest='file',
                    default='comments.json',
                    help='Defines the input file, default is comments.json')
# the next argument is needed since the 11073-10700 field identifier is broken (is _draft_d_x_r_y),
# hence it needed to be customizable
parser.add_argument('-d,', '--draft-id',
                    dest='draft_id',
                    default='_draft',
                    help='Defines the draft field identifier, default is _draft')

args = parser.parse_args()

comment_items = read_comments(args.file)

for item in comment_items:
    print(item)
    description = (
        '[this ticket has been generated automatically from a database and does not necessarily reflect'
        ' the creator\'s opinion]\n### Comment\n{}\n### Proposed change\n{}'.format(item['comment'],
                                                                                    item['proposed_change']))
    add_ticket_to_sourceforge(
        bearer=args.bearer,
        assigned_to=args.user,
        description=description,
        line=item['line'],
        page=item['page'],
        sub_clause=item['sub_clause'],
        milestone=args.milestone,
        draft=args.draft,
        must_be_satisfied=item['must_be_satisfied'],
        summary=item['summary'],
        comment_id=item['comment_id'],
        category=item['category'],
        dest=args.ticketid,
        draft_id=args.draft_id)
