import argparse
from convert_csv_to_json_lib import read_comments, write_comments

parser = argparse.ArgumentParser(description='Convert CSV comments file to JSON comments file')
parser.add_argument('-i,', '--input',
                    dest='input_file',
                    default='comments.csv',
                    help='Define input file, default is comments.csv')
parser.add_argument('-o,', '--output',
                    dest='output_file',
                    default='comments.json',
                    help='Define output file, default is comments.json')

args = parser.parse_args()
comments = read_comments(args.input_file)
write_comments(comments, args.output_file)
