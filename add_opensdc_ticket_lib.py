import json
import requests
import re


def add_ticket_to_sourceforge(bearer, assigned_to, description, summary, milestone, draft, must_be_satisfied,
                              line, page, sub_clause, comment_id, category, dest, draft_id):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Authorization': 'Bearer {}'.format(bearer)
    }
    message = {
        'access_token': bearer,
        'ticket_form.assigned_to': assigned_to,
        'ticket_form.description': description,
        'ticket_form.summary': summary,
        'ticket_form._milestone': milestone,
        'ticket_form.status': 'unread',
        'ticket_form.custom_fields.{}'.format(draft_id): draft,
        'ticket_form.custom_fields._must_be_satisfied': must_be_satisfied,
        'ticket_form.custom_fields._line': line,
        'ticket_form.custom_fields._page': page,
        'ticket_form.custom_fields._sub_clause': sub_clause,
        'ticket_form.custom_fields._general_technical_editorial': category,
        'ticket_form.custom_fields._comment_id': comment_id
    }

    print('Add ticket: {}'.format(json.dumps(message)))
    req = requests.post('https://sourceforge.net:443/rest/p/opensdc/{}/new'.format(dest), data=message, headers=headers)
    status_code = req.status_code
    if status_code == 200:
        print('Ticket successfully submitted.')
    else:
        print('Ticket could not be created. HTTP status: {}'.format(req.status_code))
        if status_code == 403:
            print('Suggested remedy: make sure the authorization token "{}" is valid.'.format(bearer))
        if status_code == 400:
            print('Suggested remedy: make sure the given milestone "{}" '
                  'matches an existing milestone on sourceforge.net'.format(milestone))

        print(req.content)


def replace_sourceforge_links(description):
    pattern = '((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)' \
              '[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-]*)?\??(?:[\-\+=&;%@\.\w]*)#?(?:[\.\!\/\\\w]*))?)'
    return re.sub(pattern, repl=r'<\1>', string=description, flags=re.IGNORECASE)


def read_comments(input_file):
    try:
        with open(input_file, 'r') as csv_file:
            comment_items = json.load(csv_file)
            print('Read {} comments'.format(len(comment_items)))
            return comment_items
    except BaseException as err:
        print('Could not read input file. Error: {}'.format(err))
        exit()
