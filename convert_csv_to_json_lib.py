import csv
import json


def convert_must_be_satisfied(must_be_satisfied):
    if must_be_satisfied == 'yes':
        must_be_satisfied = 'on'
    else:
        must_be_satisfied = 'off'
    return must_be_satisfied


def extract_summary_snippet(comment, word_count=10):
    comment_wo_linebreaks = comment.replace('\n', ' ').replace('\r', '')
    comment_words = comment_wo_linebreaks.split()
    snippet = ' '.join(comment_words[0:word_count])
    if len(comment_words) > word_count:
        snippet += '...'
    return snippet


def read_comments(input_file):
    col_count = 24
    try:
        with open(input_file, 'r') as csv_file:
            csv_lines = csv.reader(csv_file, delimiter=',', quotechar='"')
            row_index = 0
            comment_items = []
            for row in csv_lines:
                row_index += 1
                if row_index == 1:
                    continue
                if len(row) != col_count:
                    print('Invalid column count (expected {}, but actual is {}) in row {}. Skip row.'
                          .format(col_count, len(row), row_index))
                    continue

                comment = row[15]
                comment_item = {
                    'comment_id': row[2],
                    'category': row[11],
                    'page': row[12],
                    'sub_clause': row[13],
                    'line': row[14],
                    'comment': comment,
                    'must_be_satisfied': convert_must_be_satisfied(row[17]),
                    'proposed_change': row[18],
                    'summary': extract_summary_snippet(comment),
                    'author': row[3]
                }

                comment_items.append(comment_item)
                print('Parsed row {}'.format(row_index))
        return comment_items
    except BaseException as err:
        print('Could not read input file. Error: {}'.format(err))
        exit()


def write_comments(comment_items, output_file):
    data = []
    for comment_item in comment_items:
        data.append(comment_item)
        print('Write comment: {}'.format(comment_item))

    try:
        with open(output_file, 'w') as outfile:
            json.dump(data, outfile, sort_keys=True, indent=4, separators=(',', ': '))
    except BaseException as err:
        print('Could not write output file. Error{}'.format(err))
